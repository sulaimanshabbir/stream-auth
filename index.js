const express = require('express');
const StreamChat = require('stream-chat').StreamChat;
const admin = require('firebase-admin');
const cors = require('cors');

var serviceAccount = require('./toolbox-97035-firebase-adminsdk-faqq3-d775711de6.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const app = express();

app.use(express.json());
app.use(
  cors({
    origin: '*',
  })
);

app.get('/', (req, res) => {
  res.send(
    '<code>POST</code> request on <code>/createToken</code> to get a token'
  );
});

app.post('/createToken', (req, res) => {
  const apiKey = '5axk26ban77g';
  const apiSecret =
    'xdz2khx49ub8we8rsz8d7yc4kjb7aa7w6cg627epupekchwecaccnfgucsk8m3np';
  const userId = req.body.userId;

  if (!userId) {
    return res.status(400).json({ error: 'you have to send userId' });
  }

  try {
    const serverClient = StreamChat.getInstance(apiKey, apiSecret);
    const token = serverClient.createToken(userId);

    res.status(201).json({ token });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

app.post('/createTokenDev', (req, res) => {
  const userIdDev = req.body.userId;
  const apiKeyDev = req.body.apiKey;
  const apiSecretDev = req.body.apiSecret;

  if (!userIdDev || !apiKeyDev || !apiSecretDev) {
    return res.status(400).json({
      error: 'you have to send apikey, userId, apiSecret',
    });
  }

  console.log(userIdDev, apiKeyDev, apiSecretDev);

  try {
    const serverClient = StreamChat.getInstance(apiKeyDev, apiSecretDev);
    const token = serverClient.createToken(userIdDev);

    res.status(201).json({ token });
  } catch (error) {
    res.status(400).json({
      error: error.message + ' ' + 'you have to send apikey, userId, apiSecret',
    });
  }
});

app.post('/createUser', async (req, res) => {
  try {
    const { email, password, displayName } = req.body;

    const userRecord = await admin.auth().createUser({
      email,
      password,
      displayName,
    });

    res.status(201).send(userRecord);
  } catch (error) {
    console.error('Error creating user:', error);
    res.status(500).send('Error creating user');
  }
});

// app.post('/createUser', async (req, res) => {
//   try {
//     const { email, password, displayName, role } = req.body;

//     // Create user in Firebase Authentication
//     const userCredential = await createUserWithEmailAndPassword(
//       auth,
//       email,
//       password
//     );
//     const user = userCredential.user;

//     // Update user's profile
//     await updateProfile(user, { displayName });

//     // Add user to Firestore 'users' collection
//     const usersRef = collection(db, 'users');
//     const uid = user.uid;
//     await setDoc(doc(usersRef, uid), {
//       userId: uid,
//       role: role,
//       displayName: displayName,
//       email: email,
//     });

//     res.status(201).send(user);
//   } catch (error) {
//     console.error('Error creating user:', error);
//     res.status(500).send('Error creating user');
//   }
// });

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
